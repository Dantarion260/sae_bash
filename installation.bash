#!/bin/bash
#Auteur : Prades Matteo 14B
echo "nous allons proceder a une installation de prerequis d'un poste linux pour de la proggramation"
echo "voulez-vous installer visual studio code? [o/n]"
read rep
sudo apt install sl
if [ $rep = "o" ]
then
    echo "installation de visual studio code..."
    sudo snap install code --classic
    code --install-extension MS-CEINTL.vscode-language-pack-fr
    echo "voulez-vous l'extension gitlab? [o/n]"
    read rep
    if [ $rep = "o" ]
    then
        echo "installation de l'extension gitlab..."
        code --install-extension GitLab.gitlab-workflow
    fi
    echo "voulez-vous l'extension html et css? [o/n]"
    read rep
    if [ $rep = "o" ]
    then
        echo "installation de l'extension html css..."
        code --install-extension ecmel.vscode-html-css
        code --install-extension abusaidm.html-snippets
    fi
    echo "voulez-vous l'extension sql? [o/n]"
    read rep
    if [ $rep = "o" ]
    then
        echo "installation de l'extension sql..."
        code --install-extension xyz.plsql-language
    fi
    echo "voulez-vous l'extension java? [o/n]"
    read rep
    if [ $rep = "o" ]
    then
        echo "installation de l'extension java..."
        code --install-extension redhat.java
    fi
    echo "voulez-vous l'extension python? [o/n]"
    read rep
    if [ $rep = "o" ]
    then
        echo "installation de l'extension python..."
        code --install-extension ms-python.python
        code --install-extension njpwerner.autodocstring
        code --install-extension 
    fi
    echo "installation de vscode et de ces extensions terminées"
fi
echo donnez votre user name gitlab
read username
git config --global user.name $username
echo donnez votre email gitlab
read email
git config --global user.email $email
echo "voulez-vous créer un fichier pyhton? [o/n]"
read rep
if [ $rep = "o" ]
then
    mkdir ~/python
fi
echo "voulez-vous créer un fichier BD? [o/n]"
read rep
if [ $rep = "o" ]
then
    mkdir ~/BD
fi
echo "voulez-vous créer un fichier web? [o/n]"
read rep
if [ $rep = "o" ]
then
    mkdir ~/web
fi
echo "voulez-vous créer un fichier bash? [o/n]"
read rep
if [ $rep = "o" ]
then
    mkdir ~/bash
fi

echo "voulez-vous installer docker ?[o/n]"
read rep
if [ $rep = "o" ]
then
    echo "installation de docker..."
    sudo apt update
    sudo apt install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    sudo apt update
    apt-cache policy docker-ce
    sudo apt install docker-ce
fi
echo "installation de python..."
sudo apt install python3
python3 python_sae_bash.py
echo "installation de java..."
sudo apt install default-jre
sudo apt install default-jdk
javac java_hello_world.java
java java_hello_world
echo "installation terminée"
sl
